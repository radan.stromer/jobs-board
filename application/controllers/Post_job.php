<?php
class Post_job extends CI_Controller 
{
	function __Construct()
	{
		parent:: __construct();
		$this->load->model('job_model');
	}
	
	function index()
	{
		if(empty($_SESSION['user']))
		{
			$_SESSION['login_status'] ='login_needed';
			$_SESSION['redirectTo'] = 'post_job';
			//print_r($_SESSION); exit;
			if(!empty($_SESSION['redirectTo']))redirect('login');
		}
		else
		{
			$data['tab'] = 'POST JOB';
			$data['provinsi'] = $this->db->select('*')->get('provinces')->result_array();
			//sort($data['location']);
			
			$this->load->view('header',$data);
			$this->load->view('post_job',$data);
		}	
	}
 
	public function get_city($id){
		$data = $this->db->select('*')->where('province_id',$id)->get('city')->result_array();
	//	$data = $this->m_crud->get_list_one_where('city','province_id',$id,'city_title','asc');
		echo json_encode($data);
	}
	
	/*Post new job & edit existing job*/
	function submit()
	{
		date_default_timezone_set("Asia/Jakarta"); 
		if(isset($_POST['form']))
		{	
			$data = array(
			'title' => $this->security->xss_clean($_POST['title']),
			'company' => $this->security->xss_clean($_POST['company']),
			'provinsi' => $this->security->xss_clean($_POST['provinsi']),
			'kota' => $this->security->xss_clean($_POST['kota']),
			'location' => $this->security->xss_clean($_POST['location']),
			'description' => $this->security->xss_clean($_POST['description']),
			'position_level' => $this->security->xss_clean($_POST['position_level']),
			'responsibilities' => $this->security->xss_clean($_POST['responsibilities']),
			'specialization' => $this->security->xss_clean($_POST['specialization']),
			'language' => $this->security->xss_clean($_POST['language']),
			'experience' => $this->security->xss_clean($_POST['experience']),
			'education_field' => $this->security->xss_clean($_POST['education_field']),
			'study_level' => $this->security->xss_clean($_POST['study_level']),
			'skills' => $this->security->xss_clean($_POST['skills']),
			'perks' => $this->security->xss_clean($_POST['perks']),
			'salary_min' => $this->security->xss_clean(str_replace('.','',$_POST['salary_min'])),
			'salary_max' => $this->security->xss_clean(str_replace('.','',$_POST['salary_max'])),
			'duration' => $this->security->xss_clean($_POST['duration']),
			'expires' => $this->security->xss_clean($_POST['expires']),
			'created_by' => $_SESSION['user']['user_id'],
			'deleted' => 'no',
			'date_created' => date('Y-m-d H:i:s')
			);
			
			if($_POST['form_type']=='submit')
			{
				$id = $this->job_model->post_job($data);
				if(!$id)
					redirect(base_url());
				else
					redirect('jobs/view/'.$id);
			}			
			else if($_POST['form_type']=='edit')
			{
				unset($data['date_created']);
				$data['date_modified'] = date('Y-m-d H:i:s');
				$job_id = $_POST['job_id'];
				$id = $this->job_model->update_job($data,$job_id);
				if(!$id)
					redirect(base_url());
				else
					redirect('jobs/view/'.$job_id);
			}
			
		}
		else
			redirect(base_url());
		}
}
?>