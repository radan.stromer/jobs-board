<script src="<?php echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script>
	document.title = 'Jobs Board - Tambah Lowongan';
	$('body').removeClass('body-bg');
	$('body').addClass('body-bc');
	$('header').removeClass('shadow');
            

	$(document).ready(function () {
		$('#salary_min,#salary_max').mask('000.000.000.000.000', {reverse: true});
		$("#expires").datepicker({
			dateFormat: 'yy-mm-dd',
			minDate: 0
		});
		$('#post_job').submit(function(){		
			$('#responsibilities').val("<ul class='bullets'><li>"+$.trim($('#responsibilities').val()).split('\n').join('</li><li>')+"</li></ul>");
			$('#skills').val("<ul class='bullets'><li>"+$.trim($('#skills').val()).split('\n').join('</li><li>')+"</li></ul>");
			$('#perks').val("<ul class='bullets'><li>"+$.trim($('#perks').val()).split('\n').join('</li><li>')+"</li></ul>");	
		});
	
	$('#post_job').validate({
		rules:{
				title:{
					required: true,
					minlength:3,
					maxlength:80,
				},
				company:{
					required: true,
					minlength:3,
					maxlength:80,
				},
				location:{
					required: true,
				},
				position_level:{
					required: true,
				},
				description:{
					required: true,
				},
				responsibilities:{
					required: true,
				},
				skills:{
					required: true,
				},
				perks:{
					required: true,
				},
				salary_min:{
					required: true,
					lessThanEqual: '#salary_max',
				},
				salary_max:{
					required: true,
				},
				specialization:{
					required: true,
				},
				expires:{
					required: true,
				},
			},
			messages:{
				title:{
					required: "Mohon masukkan Nama Posisi dengan minimal 3 karakter.",
					minlength:"Title is too short.",
					maxlength:"Title is too long.",
				},
				company:{
					required: "Ketikkan Nama Perusahaan.",
					minlength:"Company name is too short.",
					maxlength:"Company name is too long.",
				},
				location:{
					required: "Pilih Lokasi Lowongan.",
				},
				position_level:{
					required: "Silahkan pilih posisi pekerjaan.",
				},
				description:{
					required: "Ketikkan Deskripsi Lowongan.",
				},
				responsibilities:{
					required: "Ketikkan tanggung jawab pekerjaan.",
				},
				skills:{
					required: "Ketikkan kemampuan yang diperlukan.",
				},
				perks:{
					required: "Ketikkan fasilitas dan keuntungan.",
				},
				salary_min:{
					required: "Masukkan gaji minimal.",
					lessThanEqual: "Validate min-max range.",
				},
				salary_max:{
					required: "Masukkan gaji maksimal.",					
				},
				specialization:{
					required: "Silahkan pilih spesialisasi",
				},
				expires:{
					required: "Tentukan tanggal kadarluasa.",
				},
			},
	});

	$('#post_job').submit(function(){
		if(!$('#post_job').valid())	return false;
	});
	$('#submit_job').click(function(){
		if(!$('#post_job').valid())	return false;
	});
	$('#salary_min,#salary_max').keyup(function(){
		$('#salary_min').valid();
	});

	$('#provinsi').change(function(){
		$.getJSON("<?=base_url('post_job/get_city');?>/"+$(this).val(), function(result){
		  $('#kota').empty().append("<option value=''>-Pilih Kota-</option> ");
          $.each(result, function(i, field){
            $('#kota').append("<option value='"+field.city_id+"'>"+field.city_title + "</option> ");
          });
        });
	});

	
});
	
</script>
        
 <style>
	input, select, textarea {width: 100%;resize: none;}
	p {margin: 7px 0;}
	input[type=number]{-moz-appearance:textfield}input::-webkit-inner-spin-button,input::-webkit-outer-spin-button{-webkit-appearance:none}
	.message{color: #e06c17;font-size: 14px;background: aliceblue;padding: 11px 4px;}
	.signin-a,.signup-a  {cursor:pointer;}
	.login-page {margin: auto;top: 70px;position: relative;}
	/*.form {position: relative;z-index: 1;background: #FFFFFF;padding: 7vh 5vw;text-align: center;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);}*/
	.form input textarea{outline: none;float:left;border: 1px solid #ccc;margin: 15px 0 0;padding: 15px;box-sizing: border-box;font-size: 14px;}
	.form button {font-family: "Roboto", sans-serif;text-transform: uppercase;outline: 0;background: #4CAF50;width: 100%;border: 0;margin: 15px 0 0;padding: 15px;color: #FFFFFF;font-size: 14px;-webkit-transition: all 0.3 ease;transition: all 0.3 ease;cursor: pointer;}
	.form button:hover,.form button:active,.form button:focus {background: #43A047;}
	.login_action {margin: 15px 0 0;color: #b3b3b3;font-size: 12px;}
	.login_action a {color: #4CAF50;text-decoration: none;}
	.login-page .containers {
		background: #FFFFFF;
		padding: 10px;
		margin-bottom: 1vw;
		box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
		display: -webkit-box;
	}
	.containers h2 {
		font-family: sfsemibold;
		color: #5182a7;
		font-size: 120%;
		padding: 1vw 0vw;
	}
	.form h2 {
		font-family: sfsemibold;
		font-size: 110%;
		color: #5182a7;
		text-align: left;
		padding: 1vw 0vw;
	}
	.containers .btn-primary {
		
	}
	.form .form-group label {
		color: #555;
		padding-top: 10px;
		text-align: right;
	}
	.form .form-group .col-sm-3, .form .form-group .col-sm-9 {
		padding-left: 5px;
		padding-right: 5px;
		font-size: 14px;
	}
	.form-group input.error, .form-group select.error, .form-group textarea.error {border: 1px solid #ea1d1d;}
	.form-group label.error {color: #ea1d1d;font-size: 12px;}
	.form-check-input {
		float: left;
	}
</style>

<form class="" name="post_job" id="post_job" method='post' action='<?php echo base_url();?>post_job/submit'>
<section class="login-page">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 navbar-fixed-top" style="top: 62px; padding-top: 13px; background: #dadada;">
		<div class="col-xs-0 col-sm-1 col-md-1 col-lg-1"></div>
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 row">
			<div class="containers">
				<div class="col-sm-10">
					<h2>
					<?php if($this->uri->segment(2)=='edit'){ echo "Ubah Data Lowongan"; } else { echo "Masukkan Data Lowongan";} ?>
					</h2>
				</div>
				<div class="col-sm-2">
					<?php if($this->uri->segment(2)=='edit'){ ?> 
					<input type='hidden' name='form_type' value='edit'> 
					<input type="hidden" name="job_id" value="<?=$this->uri->segment(3)?>">
					<button type="button" class="btn btn-primary" id='register'>Edit Lowongan</button>
					<?php } else { ?> 
					<input type='hidden' name='form_type' value='submit'> 
					<button type='submit' class="btn btn-primary" id='submit_job'>Pratinjau Lowongan</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-xs-0 col-sm-1 col-md-1 col-lg-1"></div>
	</div>

	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 row' style="margin-top:85px;">
		<div class="col-xs-0 col-sm-1 col-md-1 col-lg-1"></div>
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
			<div class="row containers">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="form">
						<h2>Rincian Lowongan Pekerjaan</h2>
						<hr>
						<div class="post_job col-sm-offset-1 col-sm-10">
							<div class="form-group row">
								<label class="col-sm-3">Posisi Pekerjaan</label>
								<div class="col-sm-9">
									<input type="text" name='title' id='title' class='form-control' placeholder="Contoh: Senior Web Developer"  value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->title));?><?php } ?>"/>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Jenis Pekerjaan</label>
								<div class="col-sm-9">
									<select  name='duration' id='duration' class='form-control' >
									<option value="" disabled selected hidden>-Pilih Jenis Pekerjaan-</option>
									<option value='Contract'<?php if($this->uri->segment(2)=='edit' && $edit->duration=='Contract'){ ?> selected<?php } ?>>Kontrak</option>
									<option value='Internship'<?php if($this->uri->segment(2)=='edit' && $edit->duration=='Internship'){ ?> selected<?php } ?>>Magang</option>
									<option value='Part Time'<?php if($this->uri->segment(2)=='edit' && $edit->duration=='Part Time'){ ?> selected<?php } ?>>Paruh Waktu</option>
									<option value='Full Time' <?php if($this->uri->segment(2)=='edit' && $edit->duration=='Full Time'){ ?> selected<?php } ?>>Penuh</option>
									<option value='Temporary'<?php if($this->uri->segment(2)=='edit' && $edit->duration=='Temporary'){ ?> selected<?php } ?>>Temporer</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Tingkat Jabatan</label>
								<div class="col-sm-9">
									<select name='position_level' id='position_level' class='form-control' >
									<option value="" disabled selected hidden>-Pilih Tingkat Jabatan-</option>
									<option value='CEO/GM/Director/Senior Manager' <?php if($this->uri->segment(2)=='edit' && $edit->position_level=='Full time'){ ?> selected<?php } ?>>CEO/GM/Direktur/Manajer Senior</option>
									<option value='Manager/Assistant Manager'<?php if($this->uri->segment(2)=='edit' && $edit->position_level=='Part time'){ ?> selected<?php } ?>>Manajer/Asisten Manajer</option>
									<option value='Supervisor/Coordinator'<?php if($this->uri->segment(2)=='edit' && $edit->position_level=='Part time'){ ?> selected<?php } ?>>Supervisor/Koordinator</option>
									<option value='Staff (non-management & non-supervisor)'<?php if($this->uri->segment(2)=='edit' && $edit->position_level=='Part time'){ ?> selected<?php } ?>>Pegawai (non-manajemen & non-supervisor)</option>
									<option value='Less than 1 year experience'<?php if($this->uri->segment(2)=='edit' && $edit->position_level=='Part time'){ ?> selected<?php } ?>>Lulusan baru/Pengalaman kerja kurang dari 1 tahun</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Spesialisasi Pekerjaan</label>
								<div class="col-sm-9">
									<select  name='specialization' id='specialization' class='form-control' >
									<option value="" disabled selected hidden>-Pilih Spesialisasi Pekerjaan-</option>
									<option value='Actuarial Science/Statistics' <?php if($this->uri->segment(2)=='edit' && $edit->specialization=='Actuarial Science/Statistics'){ ?> selected<?php } ?>>Actuarial Science/Statistics</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Gaji Perbulan (dalam rupiah)</label>
								<div class="col-sm-9">
									<div class='col-sm-6' style='padding: 0;padding-right: 10px'><input type="text" name='salary_min' id='salary_min' class='form-control' placeholder="Min" value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->salary_min));?><?php } ?>"/></div>
									<div class='col-sm-6' style='padding: 0;padding-left: 10px'><input type="text" name='salary_max' id='salary_max' class='form-control' placeholder="Maks"  value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->salary_max));?><?php } ?>"/></div>
								</div>
							</div>
							
							<div class="form-group row">
								<label class="col-sm-3">Tanggal Expired</label>
								<div class="col-sm-9">
									<input type="text" name='expires' id='expires' class='datepicker form-control' placeholder="Tanggal Expired"  value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->expires));?><?php }else{
									echo date('Y-m-d', strtotime("+30 days"));} ?>"/> 
								</div>
							</div> 
							
						</div>
					</div>	
				</div>
			</div>
			<div class="row containers">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="form">
						<h2>Persyaratan Kerja (untuk mendapatkan kandidat yang sesuai)</h2>
						<hr>
						<div class="post_job col-sm-offset-1 col-sm-10">
							<div class="form-group row">
								<label class="col-sm-3">Tingkat Pendidikan</label>
								<div class="col-sm-9">
									<select  name="study_level" id="study_level" class='form-control' >
									<option value="" disabled selected hidden>-Pilih Tingkat Pendidikan-</option>
									<option value='SMA'<?php if($this->uri->segment(2)=='edit' && $edit->study_level=='SMA'){ ?> selected<?php } ?>>SMA</option>
									<option value='Diploma'<?php if($this->uri->segment(2)=='edit' && $edit->study_level=='Diploma'){ ?> selected<?php } ?>>Diploma</option>
									<option value='Sarjana'<?php if($this->uri->segment(2)=='edit' && $edit->study_level=='Sarjana'){ ?> selected<?php } ?>>Gelar Sarjana</option>
									<option value='Pasca' <?php if($this->uri->segment(2)=='edit' && $edit->study_level=='Pasca'){ ?> selected<?php } ?>>Gelar Pasca Sarjana</option>
									<option value='Doktor'<?php if($this->uri->segment(2)=='edit' && $edit->study_level=='Doktor'){ ?> selected<?php } ?>>Gelar Doktor</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Bidang Pendidikan</label>
								<div class="col-sm-9">
									<select  name='education_field' id='education_field' class='form-control' >
									<option value="" disabled selected hidden>-Pilih Bidang Pendidikan-</option>
									<option value='Teknik'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Teknik'){ ?> selected<?php } ?>>Teknik</option>
									<option value='Ilmu Pengetahuan'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Ilmu Pengetahuan'){ ?> selected<?php } ?>>Ilmu Pengetahuan</option>
									<option value='Ilmu Bisnis' <?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Ilmu Bisnis'){ ?> selected<?php } ?>>Ilmu Bisnis</option>
									<option value='Jasa'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Jasa'){ ?> selected<?php } ?>>Jasa / Pelayanan</option>
									<option value='Seni'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Seni'){ ?> selected<?php } ?>>Media / Seni</option>
									<option value='Ilmu Kesehatan'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Ilmu Kesehatan'){ ?> selected<?php } ?>>Ilmu Kesehatan</option>
									<option value='Lainnya'<?php if($this->uri->segment(2)=='edit' && $edit->education_field=='Lainnya'){ ?> selected<?php } ?>>Lainnya</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Lama Pengalaman</label>
								<div class="col-sm-9">
									<select  name='experience' id='experience' class='form-control' >
									<option value="" disabled selected hidden>-Pilih Lama Pengalaman-</option>
									<option value="1"<?php if($this->uri->segment(2)=='edit' && $edit->experience=='1'){ ?> selected<?php } ?>>1 Tahun</option>
									<option value='2'<?php if($this->uri->segment(2)=='edit' && $edit->experience=='2'){ ?> selected<?php } ?>>2 Tahun</option>
									<option value='3'<?php if($this->uri->segment(2)=='edit' && $edit->experience=='3'){ ?> selected<?php } ?>>3 Tahun</option>
									<option value='4' <?php if($this->uri->segment(2)=='edit' && $edit->experience=='4'){ ?> selected<?php } ?>>4 Tahun</option>
									<option value='5'<?php if($this->uri->segment(2)=='edit' && $edit->experience=='5'){ ?> selected<?php } ?>>5 Tahun</option>
									<option value='>5'<?php if($this->uri->segment(2)=='edit' && $edit->experience=='>5'){ ?> selected<?php } ?>>Lebih dari 5 Tahun</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Keahlian</label>
								<div class="col-sm-9">
									<textarea rows='3' name='skills' id='skills' class='form-control'  placeholder="Masukkan keterampilan, pisahkan masing-masing dengan tanda koma"><?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>","\n"), array("","","","","\n"), $edit->skills));?><?php } ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Bahasa</label>
								<div class="col-sm-9">
									<input type="text" name='language' id='language' class='form-control' placeholder="Masukkan bahasa, pisahkan masing-masing dengan tanda koma"  value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->language));?><?php } ?>"/>
								</div>
							</div>

						</div>
					</div>	
				</div>
			</div>
			<div class="row containers">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="form">
						<h2>Deskripsi Pekerjaan</h2>
						<hr>

						<div class="post_job col-sm-offset-1 col-sm-10">
							<textarea rows='5' name='description' id='editor1' class='form-control' placeholder="Ketikkan Deskripsi Pekerjaan, tekan enter untuk baris baru" >
							<?php if($this->uri->segment(2)=='edit'){ ?>
							<?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>","\n"), array("","","","","\n"), $edit->description));?>
							<?php }else{ ?>
							<ul>
								<li>Kandidat harus memiliki setidaknya [Education Level]</li>
								<li>Lebih disukai [Position Level] khusus dalam [Specialization] atau setara.</li>
							</ul>
							<?php } ?>
							</textarea>
						</div>
					</div>	
				</div>
			</div>
			<div class="row containers">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="form">
						<h2>Rincian Perusahaan</h2>
						<hr>
						<div class="post_job col-sm-offset-1 col-sm-10">
							<div class="form-group row">
								<label class="col-sm-3">Nama Perusahaan</label>
								<div class="col-sm-9">
									<input type="text" name='company' id='company' class='form-control' placeholder="Ketikkan Nama Perusahaan"  value="<?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>"), array("","","",""), $edit->company));?><?php } ?>"/>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Provinsi</label>
								<div class="col-sm-9">
									<select name='provinsi' id='provinsi' class='form-control'>
									<option value='' disabled selected hidden>-Pilih Provinsi-</option>
									<?php foreach($provinsi as $key=>$val) { ?>
									<option value='<?=$val['province_id'];?>' <?php if($this->uri->segment(2)=='edit' && $edit->provinsi==$val){ ?> selected<?php } ?> ><?=$val['province_title'];?></option>
									<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Kota</label>
								<div class="col-sm-9">
									<select name='kota' id='kota' class='form-control'>
									<option value='' disabled selected hidden>-Pilih Kota-</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3">Nilai Jual</label>
								<div class="col-sm-9">
									<textarea rows='3' name='perks' id='perks' class='form-control' placeholder="Contoh. Memberikan peluang pengembangan karir, Gaji & tunjangan yang kompetitif, Budaya kerja yang inovatif & penuh semangat" ><?php if($this->uri->segment(2)=='edit'){ ?><?php  echo strip_tags(str_replace(array("<ul class='bullets'>","</ul>","<li>","</li>","\n"), array("","","","","\n"), $edit->perks));?><?php } ?></textarea>
								</div>
							</div>
							<input type='hidden' name='form' value='1'>
						</div>
					</div>	
				</div>
			</div>
		</div>
		<div class="col-xs-0 col-sm-1 col-md-1 col-lg-1"></div>
	</div>  
 
 
</section>

<script>
    CKEDITOR.replace( 'editor1' );
</script>